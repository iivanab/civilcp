# civilcp


## Installation

```bash
pip3 install flask
pip3 install requests
pip3 install pyquery

```

## Usage

```
python3 flaskapp.py
```

## Keep it simple
No Javascript framework is allowed, it includes Jquery/React/Vue etc.\
No CSS framework is allowed.


## Lint

The project must follow the [pep8 Python style guide checker](https://pypi.org/project/pep8/).\
[Here](https://code.visualstudio.com/docs/python/linting) you can learn how to activate pep8 on Visual Studio Code.

## FAQ

- Why did you make this repository?\
Because I wanted to keep things simpler.
