from flask import Flask, render_template
from app import app
from app.models.html_parser import web_scrap
import requests


@app.route('/')
@app.route('/home')
def home():
    return render_template('index.html', content="Hello World!")


@app.route('/topic')
@app.route('/topic/<path:article>')
def topic(article=None):

    if article:
        try:
            article = requests.get(article)
            article.raise_for_status()
            title, body = web_scrap(article.content)
            return render_template('topic.html', article=True, title=title, body=body)

        except requests.exceptions.MissingSchema as error:
            return 'Invalid Page!'

    return render_template('topic.html', title=article)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
