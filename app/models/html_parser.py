from pyquery import PyQuery as pq


def web_scrap(content):
    doc = pq(content)

    title = doc("h1[itemprop='headline']").text()
    body = doc("div[itemprop='articleBody']")

    return title, body
